// #include <pcl/search/impl/search.hpp>
// #include <pcl/surface/gp3.h>
// #include <pcl/features/moment_of_inertia_estimation.h>
// #include <pcl/filters/extract_indices.h>
// #include <pcl/common/angles.h> // for pcl::deg2rad
// #include <pcl/features/normal_3d.h>
// #include <pcl/console/parse.h>
// #include <pcl/filters/passthrough.h>  //Pass through filter header file
// #include <pcl/surface/concave_hull.h>
// #include <pcl/common/transforms.h>  
//typedef pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> ColorHandlerXYZ;
// #include  <pcl/filters/voxel_grid.h>
// #include <pcl/sample_consensus/method_types.h>
// #include <pcl/segmentation/sac_segmentation.h>
// #include <pcl/common/common.h>
// #include <pcl/ModelCoefficients.h>
// #include <pcl/sample_consensus/model_types.h>
// #include <pcl/filters/crop_box.h>

#include <filesystem>

#include <thread>
#include <chrono>

#include <vector>
#include <iostream>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/surface/convex_hull.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/kdtree/kdtree_flann.h>
#include  <pcl/filters/voxel_grid.h>
#include <pcl/features/moment_of_inertia_estimation.h>
#include <pcl/features/normal_3d.h>
#include <pcl/surface/gp3.h>
#include<pcl/filters/passthrough.h>  //Pass through filter header file

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

// Plane segmentation link https://pcl.readthedocs.io/en/latest/planar_segmentation.html
// Bounding box link http://codextechnicanum.blogspot.com/2015/04/find-minimum-oriented-bounding-box-of.html
// https://www.codetd.com/en/article/10835921
// https://blog.fearcat.in/a?ID=01000-f7138375-ae5a-413e-8441-68f24f047a5d

// Visualizer Tutorial: https://pointclouds.org/documentation/tutorials/pcl_visualizer.html
// AABB OOBB https://www.codetd.com/en/article/10835921
// https://codextechnicanum.blogspot.com/2015/04/find-minimum-oriented-bounding-box-of.html
// https://pointclouds.org/documentation/tutorials/pcl_visualizer.html
// http://www.diag.uniroma1.it//~nardi/Didattica/LabAI/matdid/pcl_intro.pdf
// https://pcl.readthedocs.io/projects/tutorials/en/master/statistical_outlier.html#statistical-outlier-removal
// https://programmerah.com/convex-hull-detection-and-convex-hull-vertex-solution-of-pcl-point-cloud-library-17008/
// https://pcl.readthedocs.io/projects/tutorials/en/latest/hull_2d.html
// https://pcl.readthedocs.io/projects/tutorials/en/master/bspline_fitting.html#bspline-fitting
// https://robotica.unileon.es/index.php?title=PCL/OpenNI_tutorial_3:_Cloud_processing_(advanced)


pcl::PolygonMesh  getMesh(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud) 
{
  // Acess points
  //for (int i=0;i<cloud->size(); i++) {
  //  std::cout << cloud->points[i].x  << " "    <<  cloud->points[i].y  << " "    <<  cloud->points[i].z << std::endl;
  //}

  // Normal estimation*
  pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> n;
  pcl::PointCloud<pcl::Normal>::Ptr normals (new pcl::PointCloud<pcl::Normal>);
  pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
  tree->setInputCloud (cloud);
  n.setInputCloud (cloud);
  n.setSearchMethod (tree);
  // n.setKSearch (20);
  n.setRadiusSearch (3); 
  n.compute (*normals);
  
  //viewer->addPointCloudNormals<pcl::PointXYZ,pcl::Normal>(cloud, normals,5, 0.05, "normals");

  // Concatenate the XYZ and normal fields*
  pcl::PointCloud<pcl::PointNormal>::Ptr cloud_with_normals (new pcl::PointCloud<pcl::PointNormal>);
  pcl::concatenateFields (*cloud, *normals, *cloud_with_normals);
  //* cloud_with_normals = cloud + normals

  // Crete mesh
  // Create search tree*
  pcl::search::KdTree<pcl::PointNormal>::Ptr tree2 (new pcl::search::KdTree<pcl::PointNormal>);
  tree2->setInputCloud (cloud_with_normals);

  // Initialize objects
  pcl::GreedyProjectionTriangulation<pcl::PointNormal> gp3;
  pcl::PolygonMesh triangles;

  // Set the maximum distance between connected points (maximum edge length)
  gp3.setSearchRadius (5);

  // Set typical values for the parameters
  gp3.setMu (200);
  gp3.setMaximumNearestNeighbors (100);
  gp3.setMaximumSurfaceAngle(M_PI/4); // 45 degrees
  gp3.setMinimumAngle(M_PI/18); // 10 degrees
  gp3.setMaximumAngle(2*M_PI/3); // 120 degrees
  gp3.setNormalConsistency(false);

  // Get result
  gp3.setInputCloud (cloud_with_normals);
  gp3.setSearchMethod (tree2);
  gp3.reconstruct (triangles);

  return triangles;
}

float get_distance_from_plane(float x1, float y1,float z1, float a,float b, float c,float d)
{
    // https://www.geeksforgeeks.org/distance-between-a-point-and-a-plane-in-3-d/
    d = fabs((a * x1 + b * y1 + c * z1 + d));
    float e = sqrt(a * a + b * b + c * c);
    // cout << "Perpendicular distance is " << ;
    float dd = (d/e);
    return dd;
}

pcl::PointCloud<pcl::PointXYZ>::Ptr filter(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, std::string axis, float min, float max)
{
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_after_PassThrough(new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PassThrough<pcl::PointXYZ> passthrough;
  passthrough.setInputCloud(cloud);//Input point cloud
  passthrough.setFilterFieldName(axis);//Operate on z-axis
  passthrough.setFilterLimits(min, max);//Set operation range of pass through filter
  //passthrough.setFilterLimitsNegative(true);//true means within the reserved range, false means outside the reserved range
  passthrough.filter(*cloud_after_PassThrough);//Perform filtering and save the filtering results in cloud_after_PassThrough
  return cloud_after_PassThrough;
}
 
void GetBoundingBox(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, float &lx, float &ly, float &lz, float &volume) //, pcl::visualization::PCLVisualizer::Ptr viewer) 
{
    // ---- Find bounding box
    pcl::MomentOfInertiaEstimation <pcl::PointXYZ> feature_extractor;
    feature_extractor.setInputCloud(cloud);
    feature_extractor.compute();
    std::vector <float> moment_of_inertia;  
    std::vector <float> eccentricity;  
    pcl::PointXYZ min_point_AABB;
    pcl::PointXYZ max_point_AABB;
    pcl::PointXYZ min_point_OBB;
    pcl::PointXYZ max_point_OBB;
    pcl::PointXYZ position_OBB;
    Eigen::Matrix3f rotational_matrix_OBB;
    float major_value, middle_value, minor_value; 
    Eigen::Vector3f major_vector, middle_vector, minor_vector;
    Eigen::Vector3f mass_center; 
    feature_extractor.getMomentOfInertia(moment_of_inertia);
    feature_extractor.getEccentricity(eccentricity);
    feature_extractor.getAABB(min_point_AABB,max_point_AABB); 
    feature_extractor.getOBB(min_point_OBB,max_point_OBB, position_OBB, rotational_matrix_OBB); 
    feature_extractor.getEigenValues(major_value,middle_value, minor_value); 
    feature_extractor.getEigenVectors(major_vector,middle_vector, minor_vector); 
    feature_extractor.getMassCenter(mass_center);   

    float lxAABB = max_point_AABB.x-min_point_AABB.x;
    float lyAABB = max_point_AABB.y-min_point_AABB.y;
    float lzAABB = max_point_AABB.z-min_point_AABB.z;
    float volumeAABB = lxAABB*lyAABB*lzAABB;

    Eigen::Vector3f position(position_OBB.x, position_OBB.y, position_OBB.z); 
    // std::cout<< "position_OBB: " << position_OBB << endl;
    // std::cout<< "mass_center: " << mass_center << endl;
    Eigen::Quaternionf quat(rotational_matrix_OBB);  

    float lxOBB = max_point_OBB.x-min_point_OBB.x;
    float lyOBB = max_point_OBB.y-min_point_OBB.y;
    float lzOBB = max_point_OBB.z-min_point_OBB.z;
    float volumeOBB = lxOBB*lyOBB*lzOBB;

    lx = lxOBB;
    ly = lyOBB;
    lz = lzOBB;
    volume = volumeOBB;

    //std::cout << "lx AABB: " << lxAABB << std::endl;
    //std::cout << "ly AABB: " << lyAABB << std::endl;
    //std::cout << "lz AABB: " << lzAABB << std::endl;
    //std::cout << "Volume AABB: " << volumeAABB << std::endl;
    //viewer->addCube(min_point_AABB.x,max_point_AABB.x,min_point_AABB.y,max_point_AABB.y,min_point_AABB.z,max_point_AABB.z, 1.0, 0.0, 0.0, "AABB"); 
    //viewer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_OPACITY,0.5, "AABB");  
    //viewer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, 4, "AABB");  
    //std::cout << "lx OBB: " << lxOBB << std::endl;
    //std::cout << "ly OBB: " << lyOBB << std::endl;
    //std::cout << "lz OBB: " << lzOBB << std::endl;
    //std::cout << "Volume OBB: " << volumeOBB << std::endl;
    //viewer->addCube(position,quat, max_point_OBB.x - min_point_OBB.x, max_point_OBB.y - min_point_OBB.y,max_point_OBB.z - min_point_OBB.z, "OBB");
    //viewer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR,0, 0, 1, "OBB");
    //viewer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_OPACITY,0.1, "OBB");       
    //viewer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_LINE_WIDTH,4, "OBB");
}

void compute_mean_std(std::vector<float> v, float &mean, float &stddev){
    float sum = std::accumulate(v.begin(), v.end(), 0.0);
    mean = sum / v.size();
    std::vector<float> diff(v.size());
    std::transform(v.begin(), v.end(), diff.begin(), [mean](float x) { return x - mean; });
    float sq_sum = std::inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);
    stddev = std::sqrt(sq_sum / v.size());
    //cout << "mean: " << mean << " stddev: " << stddev << endl;
}

void get_ordered_bounding_box_corners(std::vector<cv::Point2f> corners_input, cv::Point2f center, std::vector<cv::Point2f> &corners_output)
{
    // Bounding box it is a rectangle

    // Order rectagle corners clockwise
    // 0 = top left , 1 = top right, 2= bottom right 3=bottom left
    // Approach
    // Calculate the center of your rectangle.
    // Use the center of your rectangle as the new origin for your corners.
    // Convert your corners into polar coordinates.
    // Sort the corners by its angle.

    // Get new coordinates (center of the rectangle it is the origin
    cv::Point2f pp1 = corners_input[0]-center;
    cv::Point2f pp2 = corners_input[1]-center;
    cv::Point2f pp3 = corners_input[2]-center;
    cv::Point2f pp4 = corners_input[3]-center;
    // Convert to polar coordinate
    // float radius1 = sqrt((pp1.x*pp1.x)+(pp1.y*pp1.y));
    // float radius2 = sqrt((pp2.x*pp2.x)+(pp2.y*pp2.y));
    // float radius3 = sqrt((pp3.x*pp3.x)+(pp3.y*pp3.y));
    // float radius4 = sqrt((pp4.x*pp4.x)+(pp4.y*pp4.y));
    float theta1 = (atan2(pp1.y,pp1.x))*(180/M_PI);
    float theta2 = (atan2(pp2.y,pp2.x))*(180/M_PI);
    float theta3 = (atan2(pp3.y,pp3.x))*(180/M_PI);
    float theta4 = (atan2(pp4.y,pp4.x))*(180/M_PI);
    std::vector<float> ang;
    ang.push_back(theta1);
    ang.push_back(theta2);
    ang.push_back(theta3);
    ang.push_back(theta4);
    std::sort(ang.begin(),ang.end()); // comp < default

    corners_output.clear();
    for (int i=0;i<ang.size();i++)
    {
        if (ang[i]==theta1)
        {
            corners_output.push_back(corners_input[0]);

        }
        if (ang[i]==theta2)
        {
            corners_output.push_back(corners_input[1]);

        }
        if (ang[i]==theta3)
        {
            corners_output.push_back(corners_input[2]);

        }
        if (ang[i]==theta4)
        {
            corners_output.push_back(corners_input[3]);

        }
    }
}

std::vector<size_t> ordered(std::vector<float> const& values) {
    std::vector<size_t> indices(values.size());
    std::iota(begin(indices), end(indices), static_cast<size_t>(0));

    std::sort(
        begin(indices), end(indices),
        [&](size_t a, size_t b) { return values[a] < values[b]; }
    );
    return indices;
}

bool point_in_inside(cv::Point2f P, cv::Point2f A, cv::Point2f B, cv::Point2f C, cv::Point2f D) 
{
    // http://amroamroamro.github.io/mexopencv/opencv/points_polygon_demo.html
    std::vector<cv::Point2f> c;
    c.push_back(cv::Point2f(A.x,A.y));
    c.push_back(cv::Point2f(B.x,B.y));
    c.push_back(cv::Point2f(C.x,C.y));
    c.push_back(cv::Point2f(D.x,D.y));
    cv::Point2f pt(P.x,P.y);
   
    //cout << "A: " << " x: " << c[0].x << " y: " << c[0].y << endl; 
    //cout << "B: " << " x: " << c[1].x << " y: " << c[1].y << endl; 
    //cout << "C: " << " x: " << c[2].x << " y: " << c[2].y << endl; 
    //cout << "D: " << " x: " << c[3].x << " y: " << c[3].y << endl; 
    //cout << "P: " << " x: " << pt.x   << " y: " << pt.y << endl; 
    double res = cv::pointPolygonTest(c,pt,false); // 1 inside, 0 edge, -1 outside
    //cout << "res: " << res << endl;
    if (res==1) return true;
    else return false;
}

double findMedian(std::vector<int> a,  int n)
{
  
    // If size of the arr[] is even
    if (n % 2 == 0) {
  
        // Applying nth_element
        // on n/2th index
        nth_element(a.begin(),
                    a.begin() + n / 2,
                    a.end());
  
        // Applying nth_element
        // on (n-1)/2 th index
        nth_element(a.begin(),
                    a.begin() + (n - 1) / 2,
                    a.end());
  
        // Find the average of value at
        // index N/2 and (N-1)/2
        return (double)(a[(n - 1) / 2] + a[n / 2]) / 2.0;
    }
  
    // If size of the arr[] is odd
    else {
  
        // Applying nth_element
        // on n/2
        nth_element(a.begin(), a.begin() + n / 2, a.end());
  
        // Value at index (N/2)th
        // is the median
        return (double)a[n / 2];
    }
}

bool EstimateBoundingBoxAndConvexHullBox(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, float &lx, float &ly, float &lz, float &volumeHull, pcl::PointCloud<pcl::PointXYZ>::Ptr cloudf )
{

    // This functions takes a pointcloud in meters and work with meters
    float MAX_DISTANCE = 0.02; // if a point has a distance in meter less than x 0.005 is a neightboorhood point
    int IMAGE_SIZE = 3000;

    // ---------------------> Find min max along z axis
    // Zmax needs to have close to it a group of points
    float zmax = -10000; 
    float zmin = 10000; 
    // Order Z vec and store index
    std::vector<float> zvec;
    for (size_t i=0; i<cloud->points.size(); i++)
    {
        zvec.push_back( cloud->points[i].z);
    }
    sort(zvec.begin(), zvec.end() , std::greater<float>());
    std::vector<size_t> zidxs = ordered(zvec);

    //// find 10% of points
    std::vector<float> zmaxlist;
    int n_zpoints = 10;  //(int) ( (float)zvec.size()/10f ); // 10%;
    for (size_t m=0; m<zvec.size(); m++){
        zmaxlist.push_back(zvec[m]);
    }
    zmax = zmaxlist[n_zpoints/2];

    // We assume that zmax is not an isolated points
    //for (size_t m=0; m<zidxs.size(); m++)
    //{
    //    // Compute variation
    //    float temp0 = zvec[m];
    //    float temp1 = zvec[m+1];
    //    float temp2 = zvec[m+2];
    //    float temp3 = zvec[m+3];      
    //    float temp4 = zvec[m+4];
    //    float temp5 = zvec[m+5];
    //    float var = ( (temp0-temp1) + (temp0-temp2) + (temp0-temp3) + (temp0-temp4) + (temp0-temp5) ) /5.0 ;
    //    if (var<0.01)  // 1cm
    //    {
    //        zmax = temp0;
    //        break; //variation less than 1cm
    //    }
    //}

    zmin=zvec[zvec.size()-1];
    lz = zmax; // -zmin;

    // -------------------------------------------------------
    float min = 2*zmax /3.0;
    //pcl::PointCloud<pcl::PointXYZ>::Ptr cloud  =  cloud; //filter(cloud, "z", min, zmax);

    //float lxt = 0;
    //float lyt = 0;
    //float lzt = 0;
    //float volumet = 0;
    //GetBoundingBox(cloud,lxt,lyt,lz,volumet);

    // Project point on zmax
    cv::Mat topBox = cv::Mat::zeros(cv::Size(IMAGE_SIZE, IMAGE_SIZE), CV_8UC1);
    cv::Mat topBoxCol = cv::Mat::zeros(cv::Size(IMAGE_SIZE, IMAGE_SIZE), CV_8UC3);

    // KDTree functions
    // Create 2D Cloud
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud2D (new pcl::PointCloud<pcl::PointXYZ>);
    cloud2D->width = cloud->points.size();
    cloud2D->height = 1;
    cloud2D->is_dense = false;
    cloud2D->resize(cloud2D->width * cloud2D->height);
    for (size_t i=0; i<cloud->points.size(); i++)
    {
        cloud2D->points[i].x = cloud->points[i].x;
        cloud2D->points[i].y = cloud->points[i].y;
        cloud2D->points[i].z = 0; //cloud->points[i].z;
    }

    // vector<float> xt,yt,zt;
    std::vector<float> density;    
    std::vector<float> distances;    
    std::vector<cv::Point2f> points;
    pcl::KdTreeFLANN<pcl::PointXYZ> kdtree;
    kdtree.setInputCloud(cloud2D);
    int test_num = 0;
    for (size_t i=0; i<cloud->points.size(); ++i)
    {
        std::vector<float> sqr_distances;
        std::vector<int> indices;
        int n_num = kdtree.radiusSearch(cloud2D->points[i], MAX_DISTANCE, indices, sqr_distances);
        float sum =0;
        for (int k=0; k < sqr_distances.size(); k++) { sum = sum + sqr_distances[k];}
        // sum = (sum / sqr_distances.size());
        // float sum = std::accumulate(sqr_distances.begin(), sqr_distances.end(), 0);
        distances.push_back(sum);
        density.push_back((float)n_num);
    }

    // media di tutte le medie --> se tu sei distante il doppio della media
    double dmax = *max_element(distances.begin(), distances.end());
    double dmin = *min_element(distances.begin(), distances.end());
    
    //sort(density.begin(), density.end() , greater<float>());
    //float dmax = (density[0]+density[1])/2.0;
    for (size_t j=0;j<distances.size();j++)
    {
        //if (dmax >= 28) dmax = 28;
        // int d =(int)((float)density[j]*255.0)/dmax;
        float dd = ( distances[j] - dmin ) / ( dmax - dmin ) ; // 0-1
        int d = (int)((dd)*255);
        // topBox.at<uint8_t>(cv::Point(cloud->points[j].x*1000+1000,cloud->points[j].y*1000+1000)) = d;

        // min-max normalization
        float x = cloud->points[j].x*1000+1000;
        float y = cloud->points[j].y*1000+1000;
        const int RADIUS_DENS = 2;
        for (int i = -RADIUS_DENS; i < RADIUS_DENS; i++) {
          int coord_x = i + x;
          if (coord_x < 0 || coord_x > topBox.cols-1) {
            continue;
          }
          for (int k = -RADIUS_DENS; k < RADIUS_DENS; k++) {
            int coord_y = k + y;
            if (coord_y < 0 || coord_y > topBox.rows-1) {
              continue;
            }
            uchar old_val = topBox.at<uint8_t>(cv::Point(coord_x,coord_y));
            int  new_val = old_val + density[j]; //(int)std::round(10*(2.0/encoder_speed)); // velocity added!
            topBox.at<uint8_t>(cv::Point(coord_x,coord_y)) = new_val < 255 ? new_val: 255;
          }
        }
        cv::circle(topBoxCol, cv::Point(cloud->points[j].x*1000+1000,cloud->points[j].y*1000+1000), 1, cv::Scalar(255,0,0), -1);
    }

    // find the median value
    std::vector<int> data;
    int maxdata = 0;
    float mean  = 0;
    int c = 0;
    for (size_t j=0;j<distances.size();j++)
    {
        int x = cloud->points[j].x*1000+1000;
        int y = cloud->points[j].y*1000+1000;
        uchar d = topBox.at<uchar>(cv::Point(x,y));
        data.push_back(d);
        if (d> maxdata)
        {
            maxdata = d;
        }
        if (d>5) 
        {
            mean = mean + d;
            c = c +1;
        }
    }
    mean = mean / c;
    double median = findMedian(data, data.size());
    //double mean = ((double)median+double(maxdata))/2.0;
    //cout << "mean: " << mean << " median: " << median  << " maxdata: " << maxdata<< endl;
    for (size_t j=0;j<distances.size();j++)
    {
        int x = cloud->points[j].x*1000+1000;
        int y = cloud->points[j].y*1000+1000;
        uchar d = topBox.at<uchar>(cv::Point(x,y));
        if (d< mean) 
        {
            topBox.at<uint8_t>(cv::Point(x,y)) = 0;
        } 
        //else {
        //    points.push_back(cv::Point2f(cloud->points[j].x,cloud->points[j].y));
        //}
    }

    cv::normalize(topBox,topBox,0,255, cv::NORM_MINMAX);

    for (size_t j=0;j<distances.size();j++)
    {
         int x = cloud->points[j].x*1000+1000;
         int y = cloud->points[j].y*1000+1000;
         uchar d = topBox.at<uchar>(cv::Point(x,y));
         if (d>60) // TODO Check  ho avuto almeno 20 punti che sono a distanza 2 oppure su di me
         {
             points.push_back(cv::Point2f(cloud->points[j].x,cloud->points[j].y));
         }
    }

    if (points.size()<10) return false;

    cv::RotatedRect box = cv::minAreaRect(points);
    // cout << "Test : H " << box.size.height << ", W" << box.size.width  << " - Angle: " << box.angle << endl;
    cv::Point2f vtx[4];
    box.points(vtx);
    
    // Order corners!
    std::vector<cv::Point2f> corners;
    corners.clear();
    std::vector<cv::Point2f> corners_ordered;
    corners_ordered.clear();
    for( int j = 0; j < 4; j++ ) { corners.push_back(vtx[j]); }
    get_ordered_bounding_box_corners(corners,box.center,corners_ordered);
    
    // Calculate foot height and width y=Col, x=Row
    float box_height = (float)abs(corners_ordered[3].y-corners_ordered[0].y);
    float box_width = (float)abs(corners_ordered[1].x-corners_ordered[0].x);
    // Largezza=l2 - Lunghezza=l1 (Norm)
    float l1 = (float)norm(corners_ordered[1]-corners_ordered[0]);
    float l2 = (float)norm(corners_ordered[3]-corners_ordered[0]);
    lx = (float)l1; ///1000.0;
    ly = (float)l2; ///1000.0;
    
    cv::putText(topBoxCol,"P0",cv::Point(corners_ordered[0].x*1000+1000,corners_ordered[0].y*1000+1000),cv::FONT_HERSHEY_DUPLEX,1,cv::Scalar(0,255,0),1,false);
    cv::putText(topBoxCol,"P1",cv::Point(corners_ordered[1].x*1000+1000,corners_ordered[1].y*1000+1000),cv::FONT_HERSHEY_DUPLEX,1,cv::Scalar(0,255,0),1,false);
    cv::putText(topBoxCol,"P2",cv::Point(corners_ordered[2].x*1000+1000,corners_ordered[2].y*1000+1000),cv::FONT_HERSHEY_DUPLEX,1,cv::Scalar(0,255,0),1,false);
    cv::putText(topBoxCol,"P3",cv::Point(corners_ordered[3].x*1000+1000,corners_ordered[3].y*1000+1000),cv::FONT_HERSHEY_DUPLEX,1,cv::Scalar(0,255,0),1,false);
    
    cv::circle(topBoxCol,cv::Point(corners_ordered[0].x*1000+1000,corners_ordered[0].y*1000+1000),3,cv::Scalar(0,0,255),-1);
    cv::circle(topBoxCol,cv::Point(corners_ordered[1].x*1000+1000,corners_ordered[1].y*1000+1000),3,cv::Scalar(0,0,255),-1);
    cv::circle(topBoxCol,cv::Point(corners_ordered[2].x*1000+1000,corners_ordered[2].y*1000+1000),3,cv::Scalar(0,0,255),-1);
    cv::circle(topBoxCol,cv::Point(corners_ordered[3].x*1000+1000,corners_ordered[3].y*1000+1000),3,cv::Scalar(0,0,255),-1);

    cv::cvtColor(topBox, topBox, cv::COLOR_GRAY2RGB );

    cv::putText(topBox,"P0",cv::Point(corners_ordered[0].x*1000+1000,corners_ordered[0].y*1000+1000),cv::FONT_HERSHEY_DUPLEX,1,cv::Scalar(0,255,0),1,false);
    cv::putText(topBox,"P1",cv::Point(corners_ordered[1].x*1000+1000,corners_ordered[1].y*1000+1000),cv::FONT_HERSHEY_DUPLEX,1,cv::Scalar(0,255,0),1,false);
    cv::putText(topBox,"P2",cv::Point(corners_ordered[2].x*1000+1000,corners_ordered[2].y*1000+1000),cv::FONT_HERSHEY_DUPLEX,1,cv::Scalar(0,255,0),1,false);
    cv::putText(topBox,"P3",cv::Point(corners_ordered[3].x*1000+1000,corners_ordered[3].y*1000+1000),cv::FONT_HERSHEY_DUPLEX,1,cv::Scalar(0,255,0),1,false);
    
    cv::circle(topBox,cv::Point(corners_ordered[0].x*1000+1000,corners_ordered[0].y*1000+1000),3,cv::Scalar(0,0,255),-1);
    cv::circle(topBox,cv::Point(corners_ordered[1].x*1000+1000,corners_ordered[1].y*1000+1000),3,cv::Scalar(0,0,255),-1);
    cv::circle(topBox,cv::Point(corners_ordered[2].x*1000+1000,corners_ordered[2].y*1000+1000),3,cv::Scalar(0,0,255),-1);
    cv::circle(topBox,cv::Point(corners_ordered[3].x*1000+1000,corners_ordered[3].y*1000+1000),3,cv::Scalar(0,0,255),-1);

    //cv::medianBlur(topBox,topBox,9);
    cv::imwrite("topBoxCol.png",topBoxCol);
    cv::imwrite("topBox.png",topBox);

    std::vector<float> xt,yt,zt;
    for (size_t i=0; i<cloud->points.size(); i++)
    {
        bool isInside = point_in_inside(cv::Point2f(cloud->points[i].x,cloud->points[i].y),corners_ordered[0],corners_ordered[1],corners_ordered[2],corners_ordered[3]);
        if ( isInside == true ){
            xt.push_back(cloud->points[i].x);
            yt.push_back(cloud->points[i].y);
            zt.push_back(cloud->points[i].z);
        }
    }

    cloudf->width = xt.size();
    cloudf->height = 1;
    cloudf->is_dense = false;
    cloudf->resize(cloudf->width * cloudf->height);
    for (size_t i=0; i<xt.size(); i++)
    {
        cloudf->points[i].x = xt[i];
        cloudf->points[i].y = yt[i];
        cloudf->points[i].z = zt[i];
    }
    //cout << "Cloudf size: " << cloudf->points.size() << endl;
    if (cloudf->points.size()<=4) return false;
    
    // Convex hull
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_hull (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::ConvexHull<pcl::PointXYZ> chull;
    // MuteConvexHull chull;
    chull.setInputCloud (cloudf);
    chull.setComputeAreaVolume(true);
    chull.setDimension(3); 
    chull.reconstruct (*cloud_hull);
    volumeHull = chull.getTotalVolume();   

    return true;
}

void PointCloudProcessCompute(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr cloudf) 
{
    ////////////////////DownSampling ////////////////////
    pcl::VoxelGrid<pcl::PointXYZ> voxelgrid;
    voxelgrid.setInputCloud(cloud);
    float leafisize_m = 0.005f; //0.02f; //  keep every 5mm one point
    voxelgrid.setLeafSize(leafisize_m, leafisize_m, 0.02f);
    voxelgrid.filter(*cloud);

    // Create the filtering object
    //pcl::StatisticalOutlierRemoval<pcl::PointXYZ> sor;
    //sor.setInputCloud (cloud);
    //sor.setMeanK (5);
    //sor.setStddevMulThresh (1.0);
    //sor.filter (*cloud);

    float lx = 0;
    float ly = 0;
    float lz = 0;
    float volumeHull = 0;

    bool ret = EstimateBoundingBoxAndConvexHullBox(cloud, lx,ly,lz,volumeHull,cloudf);
    //lz = lz + _currentCfg.min_height/1000.0;
    if (ret==false){
        cout << "[Error]: Convex hull less than 4 points! Cloud size: " << cloud->points.size() << endl;
        // return;
    } 
    // The hull give a volum from set of points starting from min_height.
    // We assume  that this are is a parallelepipedo and we add this to the hull
    // volumeHull = volumeHull + (_currentCfg.min_height/1000.0*lx*ly);

    std::cout << "\n--------------------------" << std::endl;
    std::cout << "lx: " << lx << std::endl;
    std::cout << "ly: " << ly << std::endl;
    std::cout << "lz: " << lz << std::endl;
    std::cout << "Volume BB: " << lx*ly*lz  << std::endl;
    std::cout << "Volume Hull: " << volumeHull  << std::endl;
}

void algorithm()
{
  std::string path = "../data/test/";
  for (const auto & entry : std::filesystem::directory_iterator(path))
  {
 
    std::string filename = entry.path().stem().string();
    std::string cloudPath = "../data/test/" + filename + ".pcd";
    std::string cloudPath1 = "../data/test_sep/" + filename + "_1.pcd";
    std::string cloudPath2 = "../data/test_sep/" + filename + "_2.pcd";
    
    std::cout << cloudPath << std::endl;

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud1 (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud2 (new pcl::PointCloud<pcl::PointXYZ>);
    
    // Load pointcloud
    pcl::io::loadPCDFile<pcl::PointXYZ> (cloudPath, *cloud);
    pcl::io::loadPCDFile<pcl::PointXYZ> (cloudPath1, *cloud1);
    pcl::io::loadPCDFile<pcl::PointXYZ> (cloudPath2, *cloud2);
    
    // Process
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloudf (new pcl::PointCloud<pcl::PointXYZ>);
    PointCloudProcessCompute(cloud,cloudf);

    // save cloud
    auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);
    std::ostringstream oss;
    oss << std::put_time(&tm, "%Y-%m-%d_%H-%M-%S") << ".pcd";
    std::string name = oss.str();
    pcl::io::savePCDFileASCII(name, *cloudf);
            
    // Take points and draw them on a in image
    cv::Mat topBoxCol = cv::Mat::zeros(cv::Size(3000, 3000), CV_8UC3);
    for(int i=0; i < cloud1->points.size(); i++)
    {
        cv::circle(topBoxCol, cv::Point(cloud1->points[i].x*1000+1000,cloud1->points[i].y*1000+1000), 1, cv::Scalar(255,0,0), -1);
    }
    for(int i=0; i < cloud2->points.size(); i++)
    { 
        cv::circle(topBoxCol, cv::Point(cloud2->points[i].x*1000+1000,cloud2->points[i].y*1000+1000), 1, cv::Scalar(0,0,255), -1);
    }
    cv::line(topBoxCol, cv::Point(0+1000,cloud1->points[0].y*1000+1000),  cv::Point(cloud1->points[0].x*1000+1000,cloud1->points[0].y*1000+1000), cv::Scalar(0,126,255), 1,8,0);
    cv::line(topBoxCol, cv::Point(0+1000,cloud2->points[0].y*1000+1000),  cv::Point(cloud2->points[0].x*1000+1000,cloud2->points[0].y*1000+1000), cv::Scalar(255,25,255), 1,8,0);
    cv::line(topBoxCol, cv::Point(0+1000,cloud1->points[cloud1->points.size()-1].y*1000+1000),  cv::Point(cloud1->points[cloud1->points.size()-1].x*1000+1000,cloud1->points[cloud1->points.size()-1].y*1000+1000), cv::Scalar(0,126,255), 1,8,0);
    cv::line(topBoxCol, cv::Point(0+1000,cloud2->points[cloud2->points.size()-1].y*1000+1000),  cv::Point(cloud2->points[cloud2->points.size()-1].x*1000+1000,cloud2->points[cloud2->points.size()-1].y*1000+1000), cv::Scalar(255,25,255), 1,8,0);
    cv::imwrite("scanners.png",topBoxCol);

    // Visualization
    pcl::visualization::PCLVisualizer::Ptr viewer (new pcl::visualization::PCLVisualizer ("cloud",true));
    // Visualization
    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> red(cloud1, 255, 0, 0);
    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> blue(cloud2, 0, 0, 255);
    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> green(cloudf, 0, 255, 0);
    //pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> blue(cloud, 0, 0, 255);
    //pcl::visualization::PointCloudColorHandlerGenericField<pcl::PointXYZ> fildColor(cloud, "z"); 
    viewer->setBackgroundColor (0, 0, 0);
    viewer->addPointCloud<pcl::PointXYZ> (cloud1, red, "cloud1");
    viewer->addPointCloud<pcl::PointXYZ> (cloud2, blue, "cloud2");
    // viewer->addPolygon<pcl::PointXYZ>(cloud_hull, "hull");
    viewer->addPointCloud<pcl::PointXYZ>(cloudf, green, "hull");
    // viewer->addPolygonMesh(triangles, "triangles");  
    // viewer->addPointCloud<pcl::PointXYZ> (cloud_plane, blue, "cloud_plane");
    //viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "cloud");
    //viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "hull");
    viewer->addCoordinateSystem (1.0);
    viewer->initCameraParameters ();  
    // -----Main loop-----
    while (!viewer->wasStopped ())
    {
        viewer->spinOnce (100);
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }

  }

}

void read()
{
  std::string path = "..data/test/";
  for (const auto & entry : std::filesystem::directory_iterator(path))
  {
 
    std::string filename = entry.path().stem().string();
    std::string cloudPath = "../data/test/" + filename + ".pcd";
    std::cout << cloudPath << std::endl;

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);

    pcl::io::loadPCDFile<pcl::PointXYZ> (cloudPath, *cloud);

    pcl::visualization::PCLVisualizer::Ptr viewer (new pcl::visualization::PCLVisualizer ("cloud",true));
    // Visualization
    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> red(cloud, 255, 0, 0);
    viewer->setBackgroundColor (0, 0, 0);
    viewer->addPointCloud<pcl::PointXYZ> (cloud, red, "cloud");
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "cloud");
    viewer->addCoordinateSystem (1.0);
    viewer->initCameraParameters ();  

    // -----Main loop-----
    while (!viewer->wasStopped ())
    {
        viewer->spinOnce (100);
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        
    }
  }
}

int
main ()
{
  algorithm();
  //read();
  return (0);
}