# Volume estimation from pointcloud

```
# OpenCV
sudo apt update
sudo apt install libopencv-dev python3-opencv

# PCL  https://pointclouds.org/downloads/
sudo apt install libpcl-dev

# Build
mkdir -p pcd_to_volume/build
cd pcd_to_volume/build
cmake ..
make -j
./pcd_to_volume
```
